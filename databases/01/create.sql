CREATE TABLE departament(
id SERIAL,
name TEXT,
PRIMARY KEY (id)
);

CREATE TABLE employee(
id SERIAL,
name TEXT,
salary INT,
departament_id INT,
manager_id INT,
FOREIGN KEY (departament_id) REFERENCES departament(id),
FOREIGN KEY (manager_id) REFERENCES employee(id),
PRIMARY KEY (id)
);
