CREATE ROLE superuser;
GRANT ALL PRIVILEGES ON SCHEMA public To superuser;
CREATE USER admin;
GRANT superuser TO administrator;

CREATE ROLE readonly;
GRANT USAGE ON SCHEMA public TO readonly;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO readonly;
CREATE USER new_user;
GRANT readonly TO new_user;