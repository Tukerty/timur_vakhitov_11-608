CREATE TABLE policemans(
policeman_id SERIAL,
name TEXT,
PRIMARY KEY (policeman_id)
);

CREATE TABLE policeteams(
policeteam_id SERIAL,
policeman_1 INT,
policeman_2 INT,
policeman_3 INT,
PRIMARY KEY (policeteam_id),
FOREIGN KEY (policeman_1) REFERENCES policemans(policeman_id),
FOREIGN KEY (policeman_2) REFERENCES policemans(policeman_id),
FOREIGN KEY (policeman_3) REFERENCES policemans(policeman_id)
);

CREATE TABLE cars(
car_id SERIAL,
vin TEXT,
model TEXT,
identifier TEXT,
PRIMARY KEY (car_id)
);

CREATE TABLE criminals(
criminal_id SERIAL,
name TEXT,
address TEXT,
phone TEXT,
car_id INT,
PRIMARY KEY (criminal_id),
FOREIGN KEY (car_id) REFERENCES cars(car_id)
);

CREATE TABLE laws(
law_id SERIAL,
law_data TEXT,
cat INT,
PRIMARY KEY (law_id)
);

CREATE TABLE accidents(
accident_id SERIAL,
law_id INT,
policeteam_id INT,
criminal_id INT,
datetime TIMESTAMP,
PRIMARY KEY (accident_id),
FOREIGN KEY (law_id) REFERENCES laws(law_id),
FOREIGN KEY (criminal_id) REFERENCES criminals(criminal_id),
FOREIGN KEY (policeteam_id) REFERENCES policeteams(policeteam_id)
);

CREATE TABLE log(
log_id SERIAL,
table_name TEXT,
operation TEXT,
datetime TIMESTAMP,
PRIMARY KEY (log_id)
)

CREATE TABLE policeteams_generated(
policeteam_id SERIAL,
policeman_1_name TEXT,
policeman_2_name TEXT,
policeman_3_name TEXT,
PRIMARY KEY (policeteam_id)
);