INSERT INTO laws (law_data, cat) VALUES 
('Превышение скорости', 1),
('Отстутствие техосмотра', 2),
('Нечитаемый номер', 2),
('Управление ТС без документов', 1),
('Не пристегнут ремень безопасности', 1),
('Проезд на красный', 1),
('Ксеноновые фары', 2),
('Выезд на встречную полосу', 1),
('Отсутствие детского кресла', 1),
('Неправильная парковка', 1),
('Вождение в нетрезвом виде', 1),
('Неуплата штрафа', 2);

INSERT INTO policemans (name) VALUES
('Рогов Лукьян Аристархович'),
('Павлова Вера Тихоновна'),
('Бобылёв Василий Юлианович'),
('Комиссарова София Авдеевна'),
('Голубева Евгения Агафоновна'),
('Крюков Авдей Валерьевич'),
('Афанасьев Митрофан Егорович'),
('Богданова Евпраксия Улебовна'),
('Емельянов Олег Серапионович'),
('Доронина Прасковья Станиславовна'),
('Воронцов Ким Викторович'),
('Андреева Виктория Созоновна'),
('Наумов Всеволод Игнатьевич'),
('Носова Прасковья Святославовна'),
('Гришина Татьяна Артёмовна'),
('Крылов Христофор Донатович'),
('Ширяев Вячеслав Валерьевич'),
('Поляков Федосей Геласьевич'),
('Кудряшов Эдуард Леонидович'),
('Абрамова Ираида Валерьевна');

INSERT INTO policeteams (policeman_1, policeman_2, policeman_3) VALUES
(1, NULL, NULL),
(2,5,NULL),
(3,9,12),
(4,18,20),
(6,7,10),
(8,11,13),
(14,15,NULL),
(16,17,19);

INSERT INTO cars (vin, model, identifier) VALUES
('RU32MR913RNQ4IV7C', 'Ford Fiesta', NULL),
('FFNXYOA1ND1A6K9R8', 'Honda Fit', NULL),
('UBJDU1FIZEL8KJAXX', 'Hyundai Accent', 'Царапины на переднем бампере'),
('08CWVFLAV6J5SHUKV', 'Kia Rio', 'Ржавчина'),
('1EINFJ8M270J2QXW3', 'Nissan Versa', 'Синие полосы на крыше'),
('S88ZKPXBBCJ3ODEOU', 'Toyota Yaris',NULL),
('X5A45C989Q1MBXEKK', 'Chevy Cruze',NULL),
('44V1J3EN0DT9C7784', 'Volkswagen Passat', 'Плохая окараска'),
('CYOHPD8TLHSYRVB60', 'Ford Taurus',NULL),
('R7HIBXS03Y7PSBO6O', 'Nissan Maxima', 'Разбито лобовое стекло');

INSERT INTO criminals (name, address, phone, car_id) VALUES
('Онипченко Елисей Олегович', '163050, г. Калачинск, ул. Молодёжная, дом 27, квартира 36', '8 (927) 679-53-47', 1),
('Элинский Чеслав Адрианович', '613333, г. Александровка, ул. Зелёная, дом 73, квартира 161', '8 (903) 247-73-72', 4),
('Коржакова Лидия Назаровна', '446498, г. Петропавловское, ул. Абельмановская, дом 68, квартира 286', '8 (914) 712-18-82', 2),
('Ярмольник Доминика Брониславовна', '396431, г. Черемшаны, ул. Герцена, дом 97, квартира 74', '8 (945) 946-12-16', 3),
('Добролюбов Дементий Никанорович', '182572, г. Каменск- Шахтинский, ул. Бакунинская, дом 40, квартира 80', '8 (964) 526-29-51', 9),
('Винтухов Леондий Карлович', '446596, г. Усть-Ишим, ул. Батайская, дом 32, квартира 180', '8 (957) 138-93-28', 10),
('Тукаев Нестор Ираклиевич', '301214, г. Балахна, ул. Базовская, дом 77, квартира 290', '8 (960) 751-66-76', 6),
('Жабкин Кондрат Натанович', '678078, г. Усолье, ул. 15 лет октября, дом 11, квартира 162', '8 (922) 336-42-63', 5),
('Ясырева Мирослава Арсентиевна', '197137, г. Исянгулово, ул. Авиаторов, дом 81, квартира 210', '8 (933) 444-40-35', 8),
('Тихоход Анфиса Геннадиевна', '632791, г. Няндома, ул. Барминовская, дом 3, квартира 203', '8 (968) 894-75-66', 7);

INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (1, 3, 1,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (3, 2, 2,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (4, 1, 3,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (6, 4, 4,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (10, 5, 5,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (12, 8, 6,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (8, 4, 7,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (7, 6, 8,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (6, 3, 9,now());
INSERT INTO accidents (law_id, policeteam_id, criminal_id, datetime) VALUES (5, 2, 10,now());