CREATE FUNCTION log_accident() RETURNS trigger AS $log_accident$
    BEGIN
    	IF (TG_OP = 'DELETE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('accidents', TG_OP, now());
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('accidents', TG_OP, now());
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('accidents', TG_OP, now());
            RETURN NEW;
        END IF;
    END;
$log_accident$ LANGUAGE plpgsql;

CREATE FUNCTION log_car() RETURNS trigger AS $log_car$
    BEGIN
    	IF (TG_OP = 'DELETE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('cars', TG_OP, now());
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('cars', TG_OP, now());
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('cars', TG_OP, now());
            RETURN NEW;
        END IF;
    END;
$log_car$ LANGUAGE plpgsql;

CREATE FUNCTION log_criminal() RETURNS trigger AS $log_criminal$
    BEGIN
    	IF (TG_OP = 'DELETE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('criminals', TG_OP, now());
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('criminals', TG_OP, now());
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('criminals', TG_OP, now());
            RETURN NEW;
        END IF;
    END;
$log_criminal$ LANGUAGE plpgsql;

CREATE FUNCTION log_law() RETURNS trigger AS $log_law$
    BEGIN
    	IF (TG_OP = 'DELETE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('laws', TG_OP, now());
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('laws', TG_OP, now());
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('laws', TG_OP, now());
            RETURN NEW;
        END IF;
    END;
$log_law$ LANGUAGE plpgsql;

CREATE FUNCTION log_policeman() RETURNS trigger AS $log_policeman$
    BEGIN
    	IF (TG_OP = 'DELETE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('policemans', TG_OP, now());
    		DELETE FROM policeteams_generated;
			INSERT INTO policeteams_generated SELECT * FROM return_teams_with_names;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('policemans', TG_OP, now());
    		DELETE FROM policeteams_generated;
			INSERT INTO policeteams_generated SELECT * FROM return_teams_with_names;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('policemans', TG_OP, now());
    		DELETE FROM policeteams_generated;
			INSERT INTO policeteams_generated SELECT * FROM return_teams_with_names;
            RETURN NEW;
        END IF;
    END;
$log_policeman$ LANGUAGE plpgsql;

CREATE FUNCTION log_policeteam() RETURNS trigger AS $log_policeteam$
    BEGIN
    	IF (TG_OP = 'DELETE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('policeteams', TG_OP, now());
    		DELETE FROM policeteams_generated;
			INSERT INTO policeteams_generated SELECT * FROM return_teams_with_names;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('policeteams', TG_OP, now());
    		DELETE FROM policeteams_generated;
			INSERT INTO policeteams_generated SELECT * FROM return_teams_with_names;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO log(table_name, operation, datetime) VALUES ('policeteams', TG_OP, now());
    		DELETE FROM policeteams_generated;
			INSERT INTO policeteams_generated SELECT * FROM return_teams_with_names;
            RETURN NEW;
        END IF;
    END;
$log_policeteam$ LANGUAGE plpgsql;

CREATE TRIGGER log_accident BEFORE INSERT OR UPDATE OR DELETE ON accidents FOR EACH ROW EXECUTE PROCEDURE log_accident();
CREATE TRIGGER log_car BEFORE INSERT OR UPDATE OR DELETE ON cars FOR EACH ROW EXECUTE PROCEDURE log_car();
CREATE TRIGGER log_criminal BEFORE INSERT OR UPDATE OR DELETE ON criminals FOR EACH ROW EXECUTE PROCEDURE log_criminal();
CREATE TRIGGER log_law BEFORE INSERT OR UPDATE OR DELETE ON laws FOR EACH ROW EXECUTE PROCEDURE log_law();
CREATE TRIGGER log_policeman BEFORE INSERT OR UPDATE OR DELETE ON policemans FOR EACH ROW EXECUTE PROCEDURE log_policeman();
CREATE TRIGGER log_policeteam BEFORE INSERT OR UPDATE OR DELETE ON policeteams FOR EACH ROW EXECUTE PROCEDURE log_policeteam();