CREATE TABLE pets (id SERIAL, type TEXT, color TEXT, PRIMARY KEY (id));
INSERT INTO pets(type,color) VALUES ('dog','black'), ('cat','grey'), ('dog', 'white'), ('fox','red'), ('dog', 'black');
CREATE TABLE tmp_pets(id SERIAL PRIMARY KEY, type TEXT, color TEXT, counter BIGINT);
INSERT INTO tmp_pets(type,color,counter) SELECT type, color, COUNT(*) FROM pets GROUP BY type, color;