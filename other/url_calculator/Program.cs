﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Html_Listener
{
    class Program
    {
        static void Main(string[] args)
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://127.0.0.1:9999/");
            listener.Start();
            Console.WriteLine("Listening...");
            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                //Requesting
                var Url = context.Request.Url;

                var o = "";

                if (Url.Segments.Length > 1)
                {

                    var operation = Url.Segments[1].Replace("/", "");
                    if (operation == "add")
                    {
                        var res = 0;
                        for (int i = 2; i <= Url.Segments.Length - 1; i++)
                        {
                            res += Int32.Parse(Url.Segments[i].Replace("/", ""));
                        }
                        o = res.ToString();
                    }
                    else if (operation == "mult")
                    {
                        var res = 1;
                        for (int i = 2; i <= Url.Segments.Length - 1; i++)
                        {
                            res *= Int32.Parse(Url.Segments[i].Replace("/", ""));
                        }
                        o = res.ToString();
                    }
                    else
                    {
                        o = "Unknown operation";
                    }
                }
                else
                {
                    o = "There is no operation";
                }


                //Responding
                HttpListenerResponse response = context.Response;
                string head = "<head><style>body{font-family:sans-serif;margin:0;background: linear-gradient(to right, #ff9966, #ff5e62);display:flex;align-items: center;justify-content: center;}.container{}.text{color:#fff; width:100%; text-align:center;}.result{color:#fff;font-size:5em;width:100%;text-align:center;}</style></head>";

                string responseString = "<html>" + head + "<body><div class='container'><div class='text'>Your result is:</div></br><div class='result'>" + o + "</div></div></body></html>";
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
            }
        }
    }
}
